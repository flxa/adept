<%
Set objEMail = Server.CreateObject("CDO.Message")
Set objConfig = Server.CreateObject("CDO.Configuration")
objConfig.Fields("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objConfig.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "localhost"
objConfig.Fields("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 
objConfig.Fields("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60 
objConfig.Fields.Update
Set objEMail.Configuration = objConfig
objEMail.To = "Web_Mail@adept-it.com.au"
objEMail.From = request.form("email")
objEMail.Subject = "Support Request from Adept IT website"
strBody = strBody & "Name: " & Request.Form("name") & vbCrLf
strBody = strBody & "Company: " & Request.Form("company") & vbCrLf
strBody = strBody & "E-mail: " & Request.Form("email") & vbCrLf
strBody = strBody & "Phone: " & Request.Form("phone") & vbCrLf
strBody = strBody & "Priority: " & Request.Form("priority") & vbCrLf
strBody = strBody & "Message: " & Request.Form("description") & vbCrLf
objEMail.TextBody = strBody
objEMail.Send
Set objEMail = Nothing

response.redirect "index.html#success"
%> 