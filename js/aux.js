jQuery(document).ready(function($){
	
		var secondaryNav = $('.cd-secondary-nav');
		var contentSections = $('.cd-section');
		if (secondaryNav.offset().top) {
			var secondaryNavTopPosition = secondaryNav.offset().top;
		}
		if ($('#cd-intro-tagline').offset()) {
			var taglineOffesetTop = $('#cd-intro-tagline').offset().top + $('#cd-intro-tagline').height() + parseInt($('#cd-intro-tagline').css('paddingTop').replace('px', ''))
		} else {
			var taglineOffesetTop = 0;
		}
	if (taglineOffesetTop) {
		$(window).on('scroll', function(){
			//on desktop - assign a position fixed to logo and action button and move them outside the viewport
			( $(window).scrollTop() > taglineOffesetTop ) ? $('#cd-logo, #cd-intro-tagline .cd-btn').addClass('is-hidden') : $('#cd-logo,#cd-intro-tagline .cd-btn').removeClass('is-hidden');
			
			//on desktop - fix secondary navigation on scrolling
			if($(window).scrollTop() > secondaryNavTopPosition ) {
				//fix secondary navigation
				secondaryNav.addClass('is-fixed');
				//push the .cd-main-content giving it a top-margin
				$('.cd-main-content').addClass('has-top-margin');	
				//on Firefox CSS transition/animation fails when parent element changes position attribute
				//so we to change secondary navigation childrens attributes after having changed its position value
				setTimeout(function() {
					secondaryNav.addClass('animate-children');
					$('#cd-logo a img').addClass('fixed');
					$('#cd-logo').addClass('slide-in');
					$('#cd-intro-tagline .cd-btn').addClass('slide-in');
				}, 50);
			} else {
				secondaryNav.removeClass('is-fixed');
				$('.cd-main-content').removeClass('has-top-margin');
				setTimeout(function() {
					secondaryNav.removeClass('animate-children');
					$('#cd-logo a img').removeClass('fixed');
					$('#cd-logo').removeClass('slide-in');
					$('#cd-intro-tagline .cd-btn').removeClass('slide-in');
				}, 50);
			}

			//on desktop - update the active link in the secondary fixed navigation
			updateSecondaryNavigation();
		});
	}

	function updateSecondaryNavigation() {
		contentSections.each(function(){
			var actual = $(this),
				actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
				actualAnchor = secondaryNav.find('a[href="#'+actual.attr('id')+'"]');
			if ( ( actual.offset().top - secondaryNav.height() <= $(window).scrollTop() ) && ( actual.offset().top +  actualHeight - secondaryNav.height() > $(window).scrollTop() ) ) {
				actualAnchor.addClass('active');
			}else {
				actualAnchor.removeClass('active');
			}
		});
	}

	//on mobile - open/close secondary navigation clicking/tapping the .cd-secondary-nav-trigger
	$('.cd-secondary-nav-trigger').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('menu-is-open');
		secondaryNav.find('ul').toggleClass('is-visible');
	});

	//on mobile - open/close primary navigation clicking/tapping the menu icon
	$('.cd-primary-nav').on('click', function(event){
		if($(event.target).is('.cd-primary-nav')) $(this).children('ul').toggleClass('is-visible');
	});

	/* Make custom links scrollto */
	
	$('.scrollto').on('click', function(event){
		event.preventDefault();
		var target= $(this.hash);
		$('body,html').animate({
			'scrollTop': target.offset().top - secondaryNav.height() + 1
			}, 400
		); 
		//on mobile - close secondary navigation
		$('.cd-secondary-nav-trigger').removeClass('menu-is-open');
		secondaryNav.find('ul').removeClass('is-visible');
	});

	$('#open-modal').on('click', function(event){
		event.preventDefault();
		$('.overlay').addClass('open');
	});

	$('#close-modal').on('click', function(event){
		event.preventDefault();
		$('.overlay').removeClass('open');
	});

	$('.cd-form :required').on('focusin',function(e) {
		$(e).removeClass('error');
	});

	$('.cd-form :required').on('focusout',function(e) {
		if (this.length<1) {
			$(e).addClass('error');
		} else {
			$(e).addClass('success');
		}
	});

	$('#submit-form').on('click', function(event){
		event.preventDefault();
		var valid = false;
		$('.cd-form :required').each(function(e,el) {
			console.log(el.id+' = '+$(el).val());
			if ($(el).val().length<1) {
				$(el).addClass('error');
				$('#hint-'+el.name).html('Please fill in the ' + el.name.charAt(0).toUpperCase() + el.name.slice(1) + ' field');
			} else {
				$(el).removeClass('error');
				$(el).addClass('success');
				valid = true;
			}
		});
		if (valid) {
			$('#support-form').submit();
		}
	});

});